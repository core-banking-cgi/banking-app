package com.cgi.banking.batteries;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class JpaTest {

    @Autowired
    DataSource dataSource;

    protected Connection connection;

    protected JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void initiateConnection() throws SQLException {
        connection = DataSourceUtils.doGetConnection(dataSource);
        jdbcTemplate = new JdbcTemplate(new SingleConnectionDataSource(connection, true));
    }


    @AfterEach
    public void tearDown() throws SQLException {
        if (!connection.isClosed() && !TransactionSynchronizationManager.isActualTransactionActive()) {
            connection.close();
        }
    }
}

