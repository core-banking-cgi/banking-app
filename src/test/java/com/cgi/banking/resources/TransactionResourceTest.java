package com.cgi.banking.resources;

import com.cgi.banking.dtos.TransactionDto;
import com.cgi.banking.services.AccountService;
import com.cgi.banking.services.TransactionService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

@WebMvcTest(TransactionResource.class)
@WithMockUser(username = "test-user", roles = "ADMIN")
class TransactionResourceTest {


    @MockBean
    TransactionService transactionService;

    @Autowired
    MockMvc mockMvc ;
    @Test
    void fetchTransaction() throws Exception {

        Mockito.when(transactionService.fetchTransaction(Mockito.any(),Mockito.any()))
                .thenReturn(Mockito.mock(PageImpl.class)) ;
        mockMvc.perform(MockMvcRequestBuilders.get("/transactions")
                        .queryParam("clientName", "test"))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

    @Test
    void createTransaction() throws Exception {
        Mockito.when(transactionService.createTransaction(Mockito.any()))
                .thenReturn(Mockito.mock(TransactionDto.class)) ;
        mockMvc.perform(MockMvcRequestBuilders.post("/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {}
                                """).with(csrf()))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}