package com.cgi.banking.resources;

import com.cgi.banking.dtos.AccountCriteria;
import com.cgi.banking.dtos.AccountDto;
import com.cgi.banking.dtos.TransactionCriteria;
import com.cgi.banking.services.AccountService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;


@WebMvcTest(AccountResource.class)
@WithMockUser(username="test-user", roles = "ADMIN")
class AccountResourceTest {


    @MockBean
    AccountService accountService;

    @Autowired
    MockMvc mockMvc ;

    @Test
    void fetchSoldeCompte() throws Exception{
        Mockito.when(accountService.fetchSoldeByCompteNumber(Mockito.anyString()))
                .thenReturn(100D) ;
        mockMvc.perform(MockMvcRequestBuilders.get("/accounts/get-solde")
                .queryParam("account-number", "123"))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("100")));
    }

    @Test
    void fetchSoldeClient() throws Exception{
        Mockito.when(accountService.fetchTotalSoldeByClientId(Mockito.anyLong()))
                .thenReturn(100D) ;
        mockMvc.perform(MockMvcRequestBuilders.get("/accounts/get-client-solde")
                        .queryParam("client-id", "123"))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("100")));
    }

    @Test
    void createAccount() throws Exception{
        Mockito.when(accountService.createOrUpdateAccount(Mockito.any(AccountDto.class)))
                .thenReturn(Mockito.mock(AccountDto.class)) ;
        mockMvc.perform(MockMvcRequestBuilders.post("/accounts")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {"client": {"id": 1}}
                                """).with(csrf()))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void closeAccount() throws Exception {
        Mockito.doNothing().when(accountService).closeAccount(Mockito.any(AccountCriteria.class)) ;
        mockMvc.perform(MockMvcRequestBuilders.post("/accounts/close")
                        .content("""
                                {"accountNumber": "123", "clientId", 1}
                                """).with(csrf()))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}