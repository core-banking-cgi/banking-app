package com.cgi.banking.resources;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@Sql(statements = {"insert into client(id, firstname, lastname, birth_date) values(20, 'test_user_firstname', 'test_user_lastname', '1990-01-01') "}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_CLASS)
class ClientResourceTest {


    @Autowired
    MockMvc mockMvc;


    @Test
    @WithMockUser(value = "mocked-user", roles = "ACHAT")
    void fetchClientByCriteria() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/clients")
                        .queryParam("firstname", "test_user_firstname")
                        .queryParam("lastname", "test_user_lastname")
                        .queryParam("dateFrom", "1980-01-01")
                        .queryParam("dateTo", "1991-01-01")).andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].birthDate").value("1990-01-01"));
    }

    @Test
    @WithMockUser(value = "mocked-user", roles = "ACHAT")
    void createClient() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/clients")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                "firstname": "test_user_firstname_1",   
                                "lastname": "test_user_lastname",
                                "birthDate": "1991-01-01"
                                }
                                """))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty());

    }

    @Test
    @WithMockUser(value = "mocked-user", roles = "ACHAT")
    void deleteClient() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/clients/100"))
                .andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}