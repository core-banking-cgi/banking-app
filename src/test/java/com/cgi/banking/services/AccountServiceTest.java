package com.cgi.banking.services;

import com.cgi.banking.batteries.JpaTest;
import com.cgi.banking.dtos.AccountCriteria;
import com.cgi.banking.dtos.AccountDto;
import com.cgi.banking.dtos.ClientDto;
import jakarta.transaction.Transactional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;


@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = {"logging.level.org.hibernate.SQL=DEBUG"
})
@Sql(statements = {"insert into client(id,firstname, lastname, birth_date) values(10,'test', 'test', '1991-01-01')",
        "insert into account(id,account_number, solde, client_id) values(10,'123', 300.45, 10),(11,'124', 300.45, 10) "}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_CLASS)
class AccountServiceTest extends JpaTest {


    @Autowired
    AccountService accountService;


    @Test
    void fetchSoldeByCompteNumber() {
        Assertions.assertThat(accountService.fetchSoldeByCompteNumber("123"))
                .isEqualTo(300.45);
    }

    @Test
    void fetchTotalSoldeByClientId() {
        Assertions.assertThat(accountService.fetchTotalSoldeByClientId(10L))
                .isEqualTo(600.9);
    }


    @Test
    void createAccount() throws SQLException {
        var givenAccount = AccountDto.builder()
                .accountNumber("1234")
                .client(ClientDto.builder()
                        .firstname("my user")
                        .lastname("my user")
                        .birthDate(LocalDate.of(1990,01,01))
                        .build())
                .build();
        var createdAccount = accountService.createOrUpdateAccount(givenAccount);
        try (var preparedStatement = connection.prepareStatement("select count(*) from account where id = ?");
             var clienPreparedStatement = connection.prepareStatement("select count(*) from client where id = ?")) {
            preparedStatement.setLong(1, createdAccount.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            Assertions.assertThat(resultSet.getLong(1)).isEqualTo(1L);
            resultSet.close();


            // test Cascade Insertion
            clienPreparedStatement.setLong(1, createdAccount.getClient().getId());
            var rs = clienPreparedStatement.executeQuery();
            rs.next();
            Assertions.assertThat(rs.getLong(1)).isEqualTo(1L);
            rs.close();
            //
        } catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }

    }

    @Test
    void closeAccount() {
        AccountCriteria givenCriteria = AccountCriteria.builder()
                .accountNumber("123")
                .clientId(10L)
                .build();
        accountService.closeAccount(givenCriteria);

        Assertions.assertThat(jdbcTemplate.queryForObject("select closed from account where account_number = ?1"
                , new Object[]{givenCriteria.getAccountNumber()}, Boolean.class)).isEqualTo(Boolean.TRUE);

    }
}