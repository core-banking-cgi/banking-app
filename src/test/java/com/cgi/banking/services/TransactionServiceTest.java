package com.cgi.banking.services;

import com.cgi.banking.batteries.JpaTest;
import com.cgi.banking.dtos.TransactionCriteria;
import com.cgi.banking.dtos.TransactionDto;
import com.cgi.banking.entities.Transaction;
import com.cgi.banking.entities.TransactionStatus;
import com.cgi.banking.entities.TransactionType;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Sql(statements = {"insert into client(id,firstname, lastname, birth_date) values(15,'test', 'test', '1991-01-01')",
        "insert into account(id,account_number, closed, client_id, solde) values (15,12345, false, 15, 50) , (16,123456, false, 15, 50)"
        , "insert into transaction(id,creation_date,amount, account_id) values(15,'2024-04-13 10:00:00', 200, 15)"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_CLASS)
class TransactionServiceTest extends JpaTest {


    @Autowired
    TransactionService transactionService;


    @Test
    void createTransaction_and_except_exception_check_min_solde() {
        org.junit.jupiter.api.Assertions.assertThrows(DataIntegrityViolationException.class, () -> {
            TransactionDto givenTransaction = TransactionDto.builder()
                    .transactionType(TransactionType.WITHDRAW)
                    .transactionStatus(TransactionStatus.DONE)
                    .accountNumber("12345")
                    .clientId(15L)
                    .amount(100d)
                    .build();
            transactionService.createTransaction(givenTransaction);
        });
    }

    @Test
    void createTransaction() {
        TransactionDto givenTransaction = TransactionDto.builder()
                .transactionType(TransactionType.DEPOSIT)
                .transactionStatus(TransactionStatus.DONE)
                .accountNumber("123456")
                .clientId(15L)
                .amount(30d)
                .build();
        transactionService.createTransaction(givenTransaction);
        var result = jdbcTemplate.queryForList("select t.transaction_type from transaction t inner join account a on  a.id = t.account_id where a.account_number = ?",
                new Object[]{givenTransaction.getAccountNumber()}, TransactionType.class);
        Assertions.assertThat(result).hasSize(1);
        Assertions.assertThat(result.get(0)).isEqualTo(TransactionType.DEPOSIT);


    }

    @Test
    public void fetchTransaction() {
        TransactionCriteria criteria = TransactionCriteria.builder()
                .accountNumber("12345")
                .dateFrom(LocalDateTime.now().minusDays(5L))
                .dateTo(LocalDateTime.now())
                .clientName("test")
                .build();

        var result = transactionService.fetchTransaction(criteria, PageRequest.of(0, 10));
        Assertions.assertThat(result.getContent()).hasSize(1);

    }


}