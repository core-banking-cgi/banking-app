package com.cgi.banking.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name = "security_user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {


    @Id
    private String username;
    private String password;


    private String roles;

    private Boolean enabled;
}
