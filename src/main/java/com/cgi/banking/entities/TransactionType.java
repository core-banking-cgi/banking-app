package com.cgi.banking.entities;

public enum TransactionType {
    WITHDRAW, CREDIT , DEPOSIT;
}
