package com.cgi.banking.exceptions;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FunctionalExceptionResponse {

    private Integer code;

    private String description;

    private String severity;

    public FunctionalExceptionResponse(FunctionalException ex) {
        this.code = ex.getCode();
        this.description = ex.getDescription();
        this.severity = ex.getSeverity();
    }
}
