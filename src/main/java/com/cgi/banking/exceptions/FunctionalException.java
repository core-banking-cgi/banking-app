package com.cgi.banking.exceptions;


import lombok.Data;

@Data
public class FunctionalException extends RuntimeException {

    private Integer code;

    private String description;

    private String severity;

    public FunctionalException(Integer code, String description, String severity, Throwable e) {
        super(e);
        this.code = code;
        this.description = description;
        this.severity = severity;
    }
}
