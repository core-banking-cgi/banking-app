package com.cgi.banking.services;

import com.cgi.banking.dtos.ClientCriteria;
import com.cgi.banking.dtos.ClientDto;
import com.cgi.banking.entities.Client;
import com.cgi.banking.repositories.ClientRepository;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {


    private final ClientRepository clientRepository;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Page<ClientDto> fetchClientByCriteria(ClientCriteria criteria, Pageable pageable) {
        var result = clientRepository.findAll((root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();
            if (criteria.getFirstname() != null) {
                predicates.add(criteriaBuilder.equal(root.get("firstname"), criteria.getFirstname()));

            }
            if (criteria.getLastname() != null) {
                predicates.add(criteriaBuilder.equal(root.get("lastname"), criteria.getLastname()));

            }
            return predicates.stream().reduce(criteriaBuilder::and).orElse(null);
        }, pageable);
        return new PageImpl<>(result.stream().map(ClientDto::new)
                .collect(Collectors.toList()), pageable, result.getTotalElements());


    }

    public ClientDto createClient(ClientDto clientDto) {
        return new ClientDto(clientRepository.saveAndFlush(Client.builder()
                .id(clientDto.getId())
                .firstname(clientDto.getFirstname())
                .lastname(clientDto.getLastname())
                .birthDate(clientDto.getBirthDate())
                .build()));
    }

    public void deleteClientById(Long clientId) {
        clientRepository.deleteById(clientId);
    }
}
