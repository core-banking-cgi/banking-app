package com.cgi.banking.services;


import com.cgi.banking.dtos.AccountCriteria;
import com.cgi.banking.dtos.AccountDto;
import com.cgi.banking.entities.Account;
import com.cgi.banking.entities.Client;
import com.cgi.banking.exceptions.FunctionalException;
import com.cgi.banking.repositories.AccountRepository;
import com.cgi.banking.repositories.ClientRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class AccountService {


    private final AccountRepository accountRepository;
    private final ClientRepository clientRepository;

    public AccountService(AccountRepository accountRepository,
                          ClientRepository clientRepository) {
        this.accountRepository = accountRepository;
        this.clientRepository = clientRepository;
    }


    public Double fetchSoldeByCompteNumber(@NotNull String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(() -> new FunctionalException(1,"balance is not enough","WARN", null)).solde();
    }

    public Double fetchTotalSoldeByClientId(@NotNull Long clientId) {
        return accountRepository.fetchTotalByClientId(clientId);

    }

    @Transactional
    public AccountDto createOrUpdateAccount(AccountDto account) {


        return new AccountDto(accountRepository.saveAndFlush(Account.builder()
                        .id(account.getId())
                .client(Client.builder()
                        .id(account.getClient().getId())
                        .firstname(account.getClient().getFirstname())
                        .lastname(account.getClient().getLastname())
                        .birthDate(account.getClient().getBirthDate())
                        .build())
                .solde(account.getSolde())
                .closed(account.getClosed())
                .accountNumber(account.getAccountNumber())
                .build()));
    }


    @Transactional
    public void closeAccount(AccountCriteria criteria) {
        Assert.notNull(criteria.getClientId(), "3.clientId must not be null");
        Assert.notNull(criteria.getAccountNumber(), "4.clientNumber must not be null");
        Account account = accountRepository.findByAccountNumberAndClientId(criteria.getAccountNumber(), criteria.getClientId())
                .orElseThrow(() -> new FunctionalException(2,"Account not found for the given client","WARN",null));
        account
                .setClosed(Boolean.TRUE);
        accountRepository.saveAndFlush(account);
    }
}
