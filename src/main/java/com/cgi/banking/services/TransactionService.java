package com.cgi.banking.services;


import com.cgi.banking.dtos.TransactionCriteria;
import com.cgi.banking.dtos.TransactionDto;
import com.cgi.banking.entities.Transaction;
import com.cgi.banking.entities.TransactionStatus;
import com.cgi.banking.entities.TransactionType;
import com.cgi.banking.repositories.AccountRepository;
import com.cgi.banking.repositories.TransactionRepository;
import jakarta.persistence.criteria.Predicate;
import jakarta.transaction.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;

@Service
public class TransactionService {

    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;


    public TransactionService(TransactionRepository transactionRepository, AccountRepository accountRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
    }


    @Transactional
    @Retryable(maxAttempts = 2, backoff = @Backoff(delay = 200L, multiplier = 2))
    public TransactionDto createTransaction(TransactionDto transaction) {
        var account = accountRepository.findByAccountNumberAndClientId(transaction.getAccountNumber(), transaction.getClientId());
        Assert.isTrue(account.isPresent(), "2.account information is not correct");

        var acc = account.get();
        Assert.isTrue(!acc.getClosed(), "3.account is closed");

        return switch (transaction.getTransactionType()) {
            case WITHDRAW -> {
                acc.setSolde(acc.getSolde() - transaction.getAmount());
                accountRepository.saveAndFlush(acc);
                yield new TransactionDto(transactionRepository.saveAndFlush(Transaction.builder()
                        .transactionStatus(TransactionStatus.DONE)
                        .creationDate(LocalDateTime.now())
                        .transactionType(TransactionType.WITHDRAW)
                        .amount(transaction.getAmount())
                        .account(acc)
                        .build()));
            }
            case CREDIT -> {
                acc.setSolde(acc.getSolde() - transaction.getAmount());
                accountRepository.saveAndFlush(acc);
                yield new TransactionDto(transactionRepository.saveAndFlush(Transaction.builder()
                        .transactionStatus(TransactionStatus.CREATED)
                        .creationDate(LocalDateTime.now())
                        .transactionType(TransactionType.WITHDRAW)
                        .amount(transaction.getAmount())
                        .account(acc)
                        .build()));
            }
            case DEPOSIT -> {
                acc.setSolde(acc.getSolde() + transaction.getAmount());
                accountRepository.saveAndFlush(acc);
                yield new TransactionDto(transactionRepository.saveAndFlush(Transaction.builder()
                        .transactionStatus(TransactionStatus.DONE)
                        .transactionType(TransactionType.DEPOSIT)
                        .account(acc)
                        .creationDate(LocalDateTime.now())
                        .amount(transaction.getAmount())
                        .build()));
            }
        };
    }

    @Transactional
    public Page<TransactionDto> fetchTransaction(TransactionCriteria transactionCriteria, Pageable pageable) {
        var result = transactionRepository.findAll((root, query, criteriaBuilder) -> {
            var predicates = new ArrayList<Predicate>();
            if (transactionCriteria.getDateFrom() != null) {
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("creationDate"),
                        transactionCriteria.getDateFrom()));
            }

            if (transactionCriteria.getDateTo() != null) {
                predicates.add(criteriaBuilder.lessThan(root.get("creationDate"),
                        transactionCriteria.getDateTo()));
            }

            if (transactionCriteria.getClientName() != null) {
                predicates.add(criteriaBuilder.equal(root.join("account").join("client")
                                .get("lastname"),
                        transactionCriteria.getClientName()));
            }

            if (transactionCriteria.getAccountNumber() != null) {
                predicates.add(criteriaBuilder.equal(root.join("account").get("accountNumber"),
                        transactionCriteria.getAccountNumber()));
            }
            return predicates.stream().reduce(criteriaBuilder::and).orElse(null);
        }, pageable);
        return new PageImpl<>(result.stream().map(TransactionDto::new)
                .toList(), pageable, result.getTotalElements());
    }
}
