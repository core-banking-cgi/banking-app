package com.cgi.banking.repositories;

import com.cgi.banking.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, String> {

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameAndEnabled(String username, Boolean enabled);
}
