package com.cgi.banking.repositories;

import com.cgi.banking.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ClientRepository extends JpaRepository<Client,Long>, JpaSpecificationExecutor<Client> {
}
