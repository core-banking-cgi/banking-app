package com.cgi.banking.repositories;

import com.cgi.banking.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long>, JpaSpecificationExecutor<Account> {



    /*
           ##################### records for projection usage #################
         */
    record SoldeOnly(Double solde) {
    }

    @Query("select sum(c.solde) from Account c where c.client.id = ?1")
    Double fetchTotalByClientId(Long clientId);

    Optional<SoldeOnly> findByAccountNumber(String accountNumber);

    Optional<Account> findByAccountNumberAndClientId(String accountNumber, Long clientId);

}
