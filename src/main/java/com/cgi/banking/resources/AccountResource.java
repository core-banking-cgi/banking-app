package com.cgi.banking.resources;

import com.cgi.banking.dtos.AccountCriteria;
import com.cgi.banking.dtos.AccountDto;
import com.cgi.banking.services.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
@Slf4j
public class AccountResource {


    private final AccountService accountService;

    private AccountResource(AccountService accountService){
        this.accountService = accountService;
    }


    //
    @GetMapping("/get-solde")
    @Operation(description = "api for fetch sold of given account")
    public Double fetchSoldeCompte(@RequestParam(name = "account-number") String accountNumber) {
        log.info("fetch solde by account number: {}", accountNumber);
        return accountService.fetchSoldeByCompteNumber(accountNumber);
    }


    @GetMapping("/get-client-solde")
    public Double fetchSoldeClient(@RequestParam(name = "client-id") Long clientId) {
        log.info("fetch solde by client id: {}", clientId);
        return accountService.fetchTotalSoldeByClientId(clientId);
    }


    @PostMapping
    public AccountDto createAccount(@RequestBody @Validated AccountDto account){
        log.debug("create account for clientName : {}", account.getClient().getLastname());
        return accountService.createOrUpdateAccount(account);
    }


    @PostMapping("/close")
    public void closeAccount(AccountCriteria accountCriteria){
        log.info("close account with number: {}", accountCriteria.getAccountNumber());
        accountService.closeAccount(accountCriteria);
    }


}
