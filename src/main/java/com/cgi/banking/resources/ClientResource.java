package com.cgi.banking.resources;

import com.cgi.banking.dtos.ClientCriteria;
import com.cgi.banking.dtos.ClientDto;
import com.cgi.banking.services.ClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clients")
@Slf4j
public class ClientResource {


    private final ClientService clientService;

    public ClientResource(ClientService clientService){
        this.clientService = clientService;
    }


    @GetMapping
    public Page<ClientDto> fetchClientByCriteria(ClientCriteria clientCriteria, Pageable pageable){
        log.info("fetch client by criteria : {} , page : ({},{})", clientCriteria, pageable.first(), pageable.getPageSize());
        return clientService.fetchClientByCriteria(clientCriteria, pageable);
    }



    @PostMapping
    public ClientDto createClient(@RequestBody ClientDto clientDto){
        log.debug("create client : {}", clientDto);
        return clientService.createClient(clientDto);
    }


    @DeleteMapping("/{clientId}")
    public void deleteClient(@PathVariable("clientId") Long clientId){
        log.info("delete client by id: {}", clientId);
        clientService.deleteClientById(clientId);
    }






}
