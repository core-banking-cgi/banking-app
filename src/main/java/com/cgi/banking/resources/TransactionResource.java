package com.cgi.banking.resources;


import com.cgi.banking.dtos.TransactionCriteria;
import com.cgi.banking.dtos.TransactionDto;
import com.cgi.banking.services.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transactions")
@Slf4j
public class TransactionResource {


    private final TransactionService transactionService;


    public TransactionResource(TransactionService transactionService){
        this.transactionService = transactionService;
    }



    @GetMapping
    public Page<TransactionDto> fetchTransaction(TransactionCriteria transactionCriteria, Pageable pageable){
        log.info("fetch transaction by criteria : {} , page : ({},{})", transactionCriteria, pageable.first(), pageable.getPageSize());
        return transactionService.fetchTransaction(transactionCriteria, pageable);
    }

    @PostMapping
    public TransactionDto createTransaction(@RequestBody TransactionDto transaction){
        log.info("create transaction : {} for client : {}", transaction.getTransactionType() , transaction.getClientId());
        return transactionService.createTransaction(transaction);

    }


}
