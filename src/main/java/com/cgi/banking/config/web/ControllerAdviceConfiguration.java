package com.cgi.banking.config.web;

import com.cgi.banking.exceptions.FunctionalException;
import com.cgi.banking.exceptions.FunctionalExceptionResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.sql.SQLException;

@ControllerAdvice
public class ControllerAdviceConfiguration {


    ObjectMapper mapper = new ObjectMapper();

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException ex) throws JsonProcessingException {
        int exCode = ex.getMessage().indexOf(".");
        return ResponseEntity
                .badRequest()
                .contentType(MediaType.APPLICATION_JSON)
                .body(mapper.writeValueAsString(new FunctionalExceptionResponse(Integer.parseInt(ex.getMessage()
                        .substring(0, exCode)), ex.getMessage().substring(exCode), "WARN")));
    }

    @ExceptionHandler(SQLException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<String> handleSQLException(SQLException ex) throws JsonProcessingException {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(mapper.writeValueAsString(FunctionalExceptionResponse.builder().code(10)
                        .description(ex.getMessage()).severity("WARN").build()));
    }


    @ExceptionHandler(FunctionalException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<String> handleSQLException(FunctionalException ex) throws JsonProcessingException {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON)
                .body(mapper.writeValueAsString(ex));
    }


}
