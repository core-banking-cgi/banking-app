package com.cgi.banking.dtos;


import lombok.Data;

import java.time.LocalDate;

@Data
public class ClientCriteria {
    private String firstname;
    private String lastname;
    private LocalDate dateFrom;

    private LocalDate dateTo;
}
