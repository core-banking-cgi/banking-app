package com.cgi.banking.dtos;

import com.cgi.banking.entities.Account;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AccountDto {


    private Long id ;

    private ClientDto client;

    @NotNull
    private String accountNumber;

    private Double solde;

    private Boolean closed;


    public AccountDto(Account account) {
        this.client = new ClientDto(account.getClient());
        this.accountNumber = account.getAccountNumber();
        solde = account.getSolde();
        this.id = account.getId();
        this.closed = account.getClosed();
    }


}
