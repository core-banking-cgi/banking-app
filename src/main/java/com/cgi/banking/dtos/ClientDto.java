package com.cgi.banking.dtos;

import com.cgi.banking.entities.Client;
import lombok.*;

import java.time.LocalDate;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ClientDto {


    private Long id;

    private String firstname;
    private String lastname;
    private LocalDate birthDate;


    public ClientDto(Client client) {
        this.firstname = client.getFirstname();
        this.lastname = client.getLastname();
        this.birthDate = client.getBirthDate();
        this.id = client.getId();

    }
}
