package com.cgi.banking.dtos;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionCriteria {

    private LocalDateTime dateFrom ;

    private LocalDateTime dateTo ;

    private String accountNumber ;

    private String clientName;
}
