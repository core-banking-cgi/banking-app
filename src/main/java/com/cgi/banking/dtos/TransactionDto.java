package com.cgi.banking.dtos;

import com.cgi.banking.entities.Transaction;
import com.cgi.banking.entities.TransactionStatus;
import com.cgi.banking.entities.TransactionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDto {

    private Long id;

    private Long clientId;

    private String accountNumber;

    private Double amount;

    private TransactionStatus transactionStatus;

    private TransactionType transactionType;

    private LocalDateTime creationDate ;


    public TransactionDto(Transaction transaction) {
        this.id = transaction.getId();
        clientId = transaction.getAccount().getClient().getId();
        accountNumber = transaction.getAccount().getAccountNumber();
        amount = transaction.getAmount();
        transactionStatus = transaction.getTransactionStatus();
        transactionType = transaction.getTransactionType();
        creationDate = transaction.getCreationDate();

    }

}
