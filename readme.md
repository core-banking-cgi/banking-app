# Banking App project



## Features

-  Création d’un ou plusieurs nouveaux
-  Création de nouveaux comptes pour chacun des
-  Faire des opérations de dépôt et de retrait sur chacun des
-  Modifier les informations d’un ou plusieurs clients (Nom, Prénom, Age ...)
-  Supprimer un client

## Tech


- liquibase pour le versioning de la base de donnees
- Spring-retry pour la resilience de l'application (retry policy with fallback)
- Spring security avec basic auth integeree a swagger-ui  (JWT est possible egalement ==> necessite d'activer le HTTPS )
- Spring webmvc pour l exposition des api RSET
- @ControllerAdvice pour la gestion des exceptions/retour message en cas d'erreur'
- Spring data jpa  pour gerer la couche DAO avec les 3 options:
  - @Query pour definir les queries
  - JpaSpecicationExecutor pour le JavaDsl query
  - Derived Query Methods
- Jacoco pour  capturer la couverture de test
- Junit 5 / Mockito/ spring boot test / spring security test / TestSlices pour implementer les test unitaire (with Mocking) / integration
- Actuator pour le support du production-ready aspect:
    - readniess
    - liveness
    - metrics (fonctionnelles  + techniques ex:  monitoring des ressources )


## Installation

- mvnw celan install
- java -jar target/banking-0.0.1-SNAPSHOT

## Tester l'application

le lien pour tester les apis via swagger (http://localhost:8083/swagger-ui/index.html)
l' activation global de l'authentification (basic authentication) est possible par clique sur le boutton avec cadna "authentification"

les credentials pour se connecter sont: 
- username: user
- password: change-it

pour verifier les ressources directement dans la base vous pouvez acceder à la console de h2 via le lien (http://localhost:8083/h2-console)
les credentials pour se connecter sont: 

- username: sa
- password: change-it
   